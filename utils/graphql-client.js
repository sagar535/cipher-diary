import { GraphQLClient } from 'graphql-request';

const endpoint = 'https://graphql.fauna.com/graphql';

export const graphQLClient = (token) => {
    const secret = token || process.env.NEXT_PUBLIC_FAUNA_GUEST_SECRET;

    return new GraphQLClient(endpoint, {
        headers: {
            authorization: `Bearer ${secret}`,
        },
    });
};

export const graphQLAuthClient = () => {
    return new GraphQLClient(endpoint, {
        headers: {
            authorization: `Bearer ${process.env.NEXT_PUBLIC_AUTH_TOKEN}`,
            // 'X-Schema-Preview': 'partial-update-mutation'
        },
    });
};
