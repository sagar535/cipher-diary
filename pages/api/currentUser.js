import {getSession} from "next-auth/client"
import {adminClient} from "../../utils/fauna-client";
import {query as q} from 'faunadb'

export default async (req, res) => {
    const session = await getSession({req})
    const user = await adminClient.query(
        q.Get(q.Match(q.Index('user_by_email'), session.user.email))
    )

    console.log("ref", user.ref.id)

    res.status(200).json({id: user.ref.id, data: user.data})
}
