import { getSession } from 'next-auth/client'
import {query as q} from 'faunadb'
import {adminClient} from "../../../utils/fauna-client"


async function Notes (req, res) {
    const session = await getSession( {req} )

    console.log("session", session)

    // first fetch the user ref
    const {ref} = await adminClient.query(
        q.Get(q.Match(q.Index('user_by_email'), session.user.email))
    )

    // then fetch the diaries of the user
    const diaries = await adminClient.query(
        q.Map(
            q.Paginate(
                q.Match(
                    q.Index('diary_owner_by_user'), ref.id
                )
            ),
            q.Lambda("X", q.Get(q.Var("X")))
        )
    )

    res.status(200).json({diaries})
}

export default Notes
