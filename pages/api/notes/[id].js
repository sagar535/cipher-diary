import {query as q} from 'faunadb'
import {adminClient} from "../../../utils/fauna-client"
import {getSession} from "next-auth/client";

export default async (req, res) => {
    const id = req.query.id

    //TODO: check if note of that id belongs to current user
    const session = await getSession({req})
    const user = await adminClient.query(
        q.Get(q.Match(q.Index('user_by_email'), session.user.email))
    )

    const diary = await adminClient.query(
                        q.Get(q.Ref(q.Collection('Diary'), id))
                    )

    if (diary.data.owner == user.ref.id) {
        res.status(200).json(diary)
    } else {
        res.status(401).json({result: 'Unauthorized'})
    }

}
