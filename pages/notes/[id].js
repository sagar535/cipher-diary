import { useRouter } from 'next/router'
import useSWR from "swr"
import Layout from "../../components/Layout"
import Diary from "../../components/Diary"

const getNote = async (url) => {
    const res = await fetch(url)
    return await res.json()
}

export default () => {
    const router = useRouter()
    const id = router.query.id
    const {data, error} = useSWR(('/api/notes/'+id), getNote)

    if(error) {
        console.log(error.message)
        return (
            <div>
                <Layout/>
                <h1>Failed to load...</h1>
            </div>
        )
    }

    return (
        <div className={'font-layout'}>
            <Layout/>
            {data ? (
                <Diary diary={data}/>
            ) : (
                <p>Loading...</p>
            )}
        </div>
    )
}
