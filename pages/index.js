import Layout from "../components/Layout"
import useSWR from 'swr'
import {adminClient} from "../utils/fauna-client"
import {query as q} from 'faunadb'
import Router from "next/router";

const fetcher = async (url) => {
    const res = await fetch(url)
    return await res.json()
}

const createNewDiary = async () => {
    console.log("Creating new Diary...")

    const user = await fetcher('/api/currentUser')

    console.log(user)

    const diary = await adminClient.query(
                    q.Create(q.Collection('Diary'), {data: {
                            owner: user.id
                        }})
                )

    console.log(diary)

    Router.push('/notes/'+diary.ref.id)
}

// TODO: make sure current user is owner of the the document
const deleteNote = async (id) => {
    debugger
    console.log("Deleting the diary with id ", id)
    if (!id) {
        console.log("Diary Id not present")
        return
    }
    try {
        const diary = await adminClient.query(
            q.Delete(q.Ref(q.Collection('Diary'), id))
        )
        console.log("Deleted ", diary)
    } catch(e) {
        console.log(e.message)
    }
}

const Home = () => {
    const {data, error} = useSWR('/api/notes', fetcher)
    console.log("data", data)

    if(error) {
        console.log(error)
        return (
            <div>
                <Layout/>
                <h1>Failed to load...</h1>
            </div>
        )
    }

    return (
        <div className={'font-layout'}>
            <Layout/>
            <div className='container'>
                {
                    (data ? (
                        <div>
                            <table className='table table-font'>
                                <thead>
                                <tr>
                                    <th scope='row'>TITLE</th>
                                    <th scope='row'>Note</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                    {(data ? (
                                        data.diaries.data.map(diary => (
                                            <tr key={diary.ref['@ref'].id}>
                                                <td>
                                                        <a href={'/notes/'+diary.ref['@ref'].id}>
                                                        {diary.data.title || 'N/A'}
                                                    </a>
                                                </td>
                                                <td>{ diary.data.note && (diary.data.note.substring(0, 50) + '...') || 'N/A'}</td>
                                                <td>
                                                    <button
                                                        className={'btn btn-danger'}
                                                        onClick={() => deleteNote(diary.ref['@ref'].id)}
                                                    >
                                                        Delete
                                                    </button>
                                                </td>
                                            </tr>
                                        ))
                                    ) : (
                                        <tr>
                                            <td scope='row'><h1>LOADING...</h1></td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>

                            <button className={'btn btn-primary'} onClick={createNewDiary}>New Diary</button>
                        </div>
                    ): (
                        <h1>Loading...</h1>
                    ))
                }
            </div>
        </div>
    )
}

export default Home
