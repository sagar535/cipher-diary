Cipher diary will allow you to store your daily records ciphered with your own personal password. Your password wont be stored any where so no possibility of password jacking. To read the diary you yourself will have to enter your key.

## Getting Started

First, run the development server:

```bash
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
