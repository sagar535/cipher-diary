import {useState} from 'react'
import {query as q} from 'faunadb'
import {adminClient} from "../utils/fauna-client"
import AES from 'crypto-js/aes'
import CryptoJS from 'crypto-js'

const Diary = (diary) => {
    const ts = diary.diary.ts
    const [noteHeight, setNoteHeight] = useState('400px')
    const [encrypted, setEncrypted] = useState(diary.diary.data.note)
    const [title, setTitle] = useState(diary.diary.data.title)
    const [password, setPassword] = useState('aaaa')
    const [note, setNote] = useState('')
    const [authenticated, setAuthenticated] = useState(false)

    const noteStyle = {
        height: noteHeight
    }

    // console.log("Note", note)
    // console.log("Encrypted", encrypted)
    // console.log("Decrypted", AES.decrypt(encrypted, password).toString(CryptoJS.enc.Utf8))

    const encryptedNote = () => {
        const encrypted = AES.encrypt(note, password).toString()
        console.log(encrypted)
        return encrypted
    }

    const handlePassword = (e) => {
        const newPassword = e.target.value
        setPassword(newPassword)
        try {
            setNote(AES.decrypt(encrypted, newPassword).toString(CryptoJS.enc.Utf8))
            setAuthenticated(true)
        } catch(e) {
            if(!authenticated) setNote('')
            console.log(e.message)
        }
    }

    const handleNote = e => {
        const newNote = e.target.value
        setNote(newNote)

        const newLines = newNote.length/95
        setNoteHeight((400 + newLines * 20) + 'px')
    }

    const saveToDB = async (e) => {
        console.log("Saving to DB", diary.diary.ref['@ref'].id)
        e.preventDefault()
        try {
            const updated_diary = await adminClient.query(
                q.Update(q.Ref(q.Collection('Diary'), diary.diary.ref['@ref'].id), {data: {
                        note: encryptedNote(),
                        title: title
                    }})
            )
            console.log(updated_diary)
        } catch(e) {
            console.log(e.message)
        }
    }

    return (
        <div className={'container form-container px-100'}>
            <form>
                <div className='form-group'>
                    <label htmlFor={"title" + ts}>Title</label>
                    <input
                        className="form-control"
                        id={"title" + ts}
                        value={title}
                        onChange={e => setTitle(e.target.value)}
                        placeholder="Title Goes Here"/>
                </div>

                <div>
                    <label htmlFor={'password'+ts}>Password</label>
                    <input
                        className={'form-control'}
                        id={'password'+ts}
                        onChange={e=>handlePassword(e)}
                        type={'password'}
                        />
                </div>

                <div className='form-group'>
                    <label htmlFor={'note'+ts}>Note</label>
                    <textarea
                        className={'form-control note-font'}
                        id={'note'+ts}
                        value={note}
                        placeholder={'Jot down here...'}
                        onChange={(e) => handleNote(e)}
                        rows="13"
                        style={ noteStyle }
                        onKeyUp={(e) => handleNoteKeyUp(e)}
                    />
                </div>

                <button className={'btn btn-primary my-5'} onClick={e => saveToDB(e)}>Save</button>
            </form>
        </div>
    )
}

export default Diary
