import Header from "./Header"
import NavBar from "./NavBar"

const Layout = () => (
    <div className={'font-layout'}>
        <Header/>
        <NavBar/>
    </div>
)

export  default  Layout
