import Head from 'next/head'

const Header = () => (
    <Head>
        <title>Cipher Diary</title>
        <meta name="description" content="Diary ciphered with password" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
    </Head>
)

export default Header
