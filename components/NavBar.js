import { signIn, signOut, useSession } from 'next-auth/client'
import Link from 'next/link'
import {guestClient} from "../utils/fauna-client"
import {query as q} from 'faunadb'

const NavBar = () => {
    const [session, loading] = useSession()

    // checking if user is present
    if(session) {
        try {
            // checks if user present in Remote DB
            guestClient.query(
                // Exists returns boolean, Casefold returns normalize string
                q.Exists(q.Match(q.Index('user_by_email'), q.Casefold(session.user.email)))
            ).then((emailExists) => {
                // case when user doesnot exists in remote DB
                if (!emailExists) {
                    // creating new user
                    guestClient.query(
                        q.Create(q.Collection('User'), {
                            data: {
                                email: session.user.email,
                                name: session.user.name
                            }
                        })
                    ).then((response) => {
                        console.log(response)
                    })
                }
            })
        } catch(e) {
            console.log(e.message)
        }
    }

    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <a className="navbar-brand ms-3" href="/">Diaries</a>

                <div className="ms-auto" id="navbarSupportedContent">

                    <div className="mx-auto">
                        { !session && (
                                <a
                                    className="btn btn-primary mx-3"
                                    href="/api/auth/signin"
                                    onClick={(e) => {
                                       e.preventDefault();
                                       signIn();}}
                                >
                                    LOG IN
                                </a>
                            ) }

                        {session && (
                            <>
                                <Link href="/profile">
                                    <a className="mx-3">
                                        <span
                                          style={{ backgroundImage: `url(${session.user.image})` }}
                                          className="avatar"
                                        />
                                    </a>
                                </Link>
                                {/*<span className="email">{session.user.email}</span>*/}
                                <a
                                    class="ms-auto"
                                    href="/api/auth/signout"
                                    onClick={(e) => {
                                        e.preventDefault();
                                        signOut();
                                    }}
                                >
                                    <button className="signOutButton btn btn-danger">Sign out</button>
                                </a>
                            </>
                        )}
                    </div>

                    <div className="form-inline my-2 my-lg-0 ms-auto me-3 d-none">
                        <div className="d-flex">
                            <input className="form-control mr-sm-2 me-3" type="search" placeholder="Search Diaries" aria-label="Search"></input>
                            <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    )
}

export default NavBar
